import { Container, Row, Col, Label, Input, Button } from "reactstrap";

import Abc from "../assets/images/abc.JPG"

const Information = () => {
    return(
        <Container className="p-5">
            <Row >
                <Col className="">
                    <h4 className="text-center">Hồ sơ nhân viên</h4>
                </Col>
            </Row>
            <Row className="">
                <Col className="col-8">
                    <Col className="form-group">
                        <Row className="mt-4">
                            <Col className="col-3">
                                <Label>Họ và tên:</Label>
                            </Col>
                            <Col className="col-9">
                                <Input />
                            </Col>
                        </Row>
                    </Col>
                    <Col className="form-group">
                        <Row className="mt-4">
                            <Col className="col-3">
                                <Label>Ngày sinh:</Label>
                            </Col>
                            <Col className="col-9">
                                <Input />
                            </Col>
                        </Row>
                    </Col>
                    <Col className="form-group">
                        <Row className="mt-4">
                            <Col className="col-3">
                                <Label>Số điện thoại:</Label>
                            </Col>
                            <Col className="col-9">
                                <Input />
                            </Col>
                        </Row>
                    </Col>
                    <Col className="form-group">
                        <Row className="mt-4">
                            <Col className="col-3">
                                <Label>Giới tính:</Label>
                            </Col>
                            <Col className="col-9">
                                <Input />
                            </Col>
                        </Row>
                    </Col>
                </Col>
                <Col className="col-4">
                    <Row className="justify-content-center">
                        <img src={Abc} style={{maxHeight: "300px", width: "200px", padding: "0"}}/>
                    </Row>
                </Col>
            </Row>
            <Row className="mt-4">
                <Col className="form-group col-2">
                    <Label>Công việc:</Label>
                </Col>
                <Col className="form-group col-10">
                    <textarea className="form-control">

                    </textarea>
                </Col>
            </Row>
            <Row className="mt-3 justify-content-end">
                <Button style={{width:"100px"}} color="warning">Chi tiết</Button>
                <Button style={{width:"100px"}} color="success">Kiểm tra</Button>
            </Row>
        </Container>
    )
}

export default Information;