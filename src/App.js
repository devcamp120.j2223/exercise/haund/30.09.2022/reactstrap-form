import "bootstrap/dist/css/bootstrap.min.css"
import Information from "./components/information";

function App() {
  return (
    <div>
      <Information />
    </div>
  );
}

export default App;
